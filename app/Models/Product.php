<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama', 'harga', 'stock', 'berat', 'gambar', 'gender_id'
    ];

    public function gender() {
        return $this->belongsTo(Gender::class, 'gender_id', 'id');
    }

    public function pesanan_details() {
        return $this->hasMany(PesananDetail::class, 'product_id', 'id');
    }
}
