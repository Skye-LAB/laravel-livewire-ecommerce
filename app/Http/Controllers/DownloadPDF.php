<?php

namespace App\Http\Controllers;

use App\Models\LaporanView;
use Illuminate\Http\Request;
use PDF;

class DownloadPDF extends Controller
{
    public function createPDFAll()
    {

        $pdf = PDF::loadView('download-pdf', [
            'laporans' => LaporanView::get()
        ]);


        return $pdf->stream("filename.pdf", array("Attachment" => false));
        // return view('download-pdf', [ 'laporans' => LaporanView::get() ]);
    }

    public function createPDFMonthly($month)
    {

        $pdf = PDF::loadView('download-pdf', [
            'laporans' => LaporanView::whereMonth('updated_at', $month)->get()
        ]);


        return $pdf->stream("filename.pdf", array("Attachment" => false));
        // return view('download-pdf', [ 'laporans' => LaporanView::get() ]);
    }
}
