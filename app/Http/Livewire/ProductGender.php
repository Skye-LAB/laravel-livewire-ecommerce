<?php

namespace App\Http\Livewire;

use App\Models\Gender;
use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;

class ProductGender extends Component
{
    use WithPagination;

    public $search, $gender;

    protected $updateQueryString = ['search'];

    public function mount($genderId) {
        $genderDetail = Gender::find($genderId);

        if ($genderDetail) {
            $this->gender = $genderDetail;
        }
    }

    public function render() {
        if ($this->search) {
            $product = Product::where('gender_id', $this->gender->id)->where('nama', 'like', '%' . $this->search . '%')->paginate(8);
        } else {
            $product = Product::where('gender_id', $this->gender->id)->paginate(8);
        }
        return view('livewire.product-index', [
            'products' => $product,
            'title' => 'Sepatu ' . ucwords($this->gender->nama)
        ]);
    }

    public function updatingSearch() {
        $this->resetPage();
    }
}
