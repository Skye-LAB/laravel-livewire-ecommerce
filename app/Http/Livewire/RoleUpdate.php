<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class RoleUpdate extends Component
{
    public function jadiAdmin($id) {
        $user = User::where('id', $id)->first();
        if ($user->level == 'pegawai') {
            $user->level = 'admin';
            $user->update();

            session()->flash('message', 'Pegawai Telah Dijadikan Admin!!');
        } else if ($user->level == 'user'){
            $user->level = 'pegawai';
            $user->update();

            session()->flash('message', 'User Telah Dijadikan Pegawai!!');
        }

    }

    public function delete($id) {
        $user = User::where('id', $id)->first();

        if ($user->level == 'user') {
            $user->delete();

            session()->flash('warning', 'User dihapus!!');
        } else if ($user->level == 'pegawai') {
            $user->delete();

            session()->flash('warning', 'Pegawai dihapus!!');
        } else {
            $user->delete();

            session()->flash('warning', 'Admin dihapus!!');
        }
    }

    public function render()
    {
        return view('livewire.role-update', [
            'pegawais' => User::get()
        ]);
    }
}
