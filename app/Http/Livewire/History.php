<?php

namespace App\Http\Livewire;

use App\Models\Pesanan;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class History extends Component
{
    public $pesanan;

    public function render()
    {
        if (Auth::class) {
            $this->pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status', '!=', 0)->get();
        }

        return view('livewire.history', [
            'pesanans' => $this->pesanan
        ]);
    }
}
