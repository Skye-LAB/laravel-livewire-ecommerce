<?php

namespace App\Http\Livewire;

use App\Models\Gender;
use App\Models\Pesanan;
use App\Models\PesananDetail;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Navbar extends Component
{
    public $jumlah = 0, $pembelians = 0;

    protected $listeners = [
        'masukKeranjang' => 'handleKeranjang'
    ];

    public function mount()
    {
        if (Auth::user()) {
            $pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status', 0)->first();

            $this->pembelians = Pesanan::where('status', 1)->get();

            if ($pesanan) {
                $this->jumlah = PesananDetail::where('pesanan_id', $pesanan->id)->count();
            } else {
                $this->jumlah = 0;
            }
        }
    }

    public function render()
    {
        return view('livewire.navbar', [
            'genders' => Gender::all(),
            'jumlah_pesanan' => $this->jumlah,
            'pembelians' => $this->pembelians
        ]);
    }

    public function handleKeranjang() {
        if (Auth::user()) {
            $pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status', 0)->first();

            $this->pembelians = Pesanan::where('status', 1)->get();

            if ($pesanan) {
                $this->jumlah = PesananDetail::where('pesanan_id', $pesanan->id)->count();
            } else {
                $this->jumlah = 0;
            }
        }
    }
}
