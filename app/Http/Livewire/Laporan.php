<?php

namespace App\Http\Livewire;

use App\Models\LaporanView;
use Livewire\Component;

class Laporan extends Component
{
    public $bulan = 1;

    public function render()
    {
        return view('livewire.laporan', [
            'laporans' => LaporanView::get()
        ]);
    }
}
