<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;

class ListProduct extends Component
{
    public function render()
    {
        return view('livewire.list-product', [
            'products' => Product::get()
        ]);
    }

    public function delete($id) {
        $product = Product::where('id', $id)->first();

        unlink(storage_path('app/public/photos/' . $product->gambar));

        $product->delete();

        session()->flash('message', 'Product Dihapus!!');
    }
}
