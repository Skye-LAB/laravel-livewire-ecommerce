<?php

namespace App\Http\Livewire;

use App\Models\Pesanan;
use App\Models\PesananDetail;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ProductDetail extends Component
{
    public $product, $jumlah_pesanan;

    public function mount($id)
    {
        $productDetail = Product::find($id);

        if ($productDetail) {
            $this->product = $productDetail;
        }
    }

    public function render()
    {
        return view('livewire.product-detail');
    }

    public function masukkanKeranjang()
    {
        $this->validate([
            'jumlah_pesanan' => 'required'
        ]);

        if (!Auth::user()) {
            return redirect()->route('login');
        }

        if ($this->jumlah_pesanan > $this->product->stock) {

            session()->flash('warning', 'Maaf, Stock tidak memadai');
        } else {

            $total_harga = $this->jumlah_pesanan * $this->product->harga;

            $pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status', 0)->first();

            if (empty($pesanan)) {
                Pesanan::create([
                    'user_id' => Auth::user()->id,
                    'total_harga' => $total_harga,
                    'status' => 0
                ]);

                $pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status', 0)->first();
                $pesanan->kode_pesanan = 'LR-' . $pesanan->id;
                $pesanan->update();
            } else {
                $pesanan->total_harga += $total_harga;
                $pesanan->update();
            }

            PesananDetail::create([
                'product_id' => $this->product->id,
                'pesanan_id' => $pesanan->id,
                'jumlah_pesanan' => $this->jumlah_pesanan,
                'total_harga' => $total_harga
            ]);

            $this->product->stock -= $this->jumlah_pesanan;
            $this->product->update();

            $this->emit('masukKeranjang');

            session()->flash('message', 'Sukses Masuk Keranjang');

            return redirect()->back();
        }
    }
}
