<?php

namespace App\Http\Livewire;

use App\Models\Gender;
use App\Models\Product;
use Livewire\Component;
use Livewire\WithFileUploads;

class TambahProduct extends Component
{

    use WithFileUploads;

    public $title = 'Tambah Product', $nama, $gender, $harga, $stock, $berat, $oldPhoto, $photo, $product;

    public function save()
    {

        $this->validate([
            'nama' => 'required|min:6',
            'gender' => 'required',
            'harga' => 'required',
            'stock' => 'required',
            'berat' => 'required',
            'photo' => 'image|max:2048',
        ]);

        if ($this->photo != '' && $this->photo != null) {
            $this->photo->store('photos' , 'public');
        }

        $genderId = Gender::where('nama', $this->gender)->first();

        Product::create([
            'nama' => $this->nama,
            'harga' => $this->harga,
            'stock' => $this->stock,
            'berat' => $this->berat,
            'gambar' => $this->photo->hashName(),
            'gender_id' => $genderId->id
        ]);

        $this->resetForm();

        session()->flash('message', 'Product Ditambahkan!!');

    }

    public function edit($id) {
        $this->validate([
            'nama' => 'required|min:6',
            'gender' => 'required',
            'harga' => 'required',
            'stock' => 'required',
            'berat' => 'required',
            'photo' => 'max:2048',
        ]);

        if ($this->photo != $this->oldPhoto && $this->photo != '' && $this->photo != null) {
            $old = storage_path('app/public/photos/' . $this->oldPhoto);

            if ($old) {
                unlink($old);
            }
        }

        if ($this->photo != '' && $this->photo != null) {
            $this->photo->store('photos' , 'public');
        }

        $genderId = Gender::where('nama', $this->gender)->first();

        Product::where('id', $id)->update([
            'nama' => $this->nama,
            'harga' => $this->harga,
            'stock' => $this->stock,
            'berat' => $this->berat,
            'gambar' => $this->photo ? $this->photo->hashName() : $this->oldPhoto,
            'gender_id' => $genderId->id
        ]);

        $this->resetForm();

        session()->flash('message', 'Product Diubah!!');

        return redirect()->route('list.product');
    }

    public function resetForm() {
        $this->nama = "";
        $this->gender = "Male";
        $this->harga = "";
        $this->stock = "";
        $this->berat = "";
        $this->photo = null;
        $this->oldPhoto = null;
    }

    public function updatedNama()
    {
        $this->validate([
            'nama' => 'required|min:6'
        ]);
    }

    public function updatedPhoto()
    {
        $this->validate([
            'photo' => 'image|max:2048',
        ]);
    }

    public function mount($id = null) {
        if ($id) {
            $this->title = 'Edit Product';
            $this->product = Product::where('id', $id)->first();
            $this->nama = $this->product->nama;
            $this->gender = $this->product->gender->nama;
            $this->stock = $this->product->stock;
            $this->harga = $this->product->harga;
            $this->berat = $this->product->berat;
            $this->oldPhoto = $this->product->gambar;
        }
    }

    public function render($id = null)
    {
        if ($id) {
            dd('hello');
        }

        return view('livewire.tambah-product', [
            'genders' => Gender::get()
        ]);
    }
}
