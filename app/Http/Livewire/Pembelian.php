<?php

namespace App\Http\Livewire;

use App\Models\Pesanan;
use Livewire\Component;

class Pembelian extends Component
{
    public function render()
    {
        // if (Auth::user()) {
        //     $this->pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status', 0)->first();

        //     if ($this->pesanan) {
        //         $this->pesanan_details = PesananDetail::where('pesanan_id', $this->pesanan->id)->get();
        //     }
        // }

        return view('livewire.pembelian', [
            'pesanans' => Pesanan::get(),
        ]);
    }

    public function done($id) {
        $pesanan = Pesanan::where('id', $id)->first();
        $pesanan->status = 2;
        $pesanan->update();

        $this->emit('masukKeranjang');

        session()->flash('message', 'Pesanan ' . $pesanan->kode_pesanan . ' telah lunas');
    }
}
