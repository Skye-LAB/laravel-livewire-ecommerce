<!doctype html>
<html lang="">

<head>
    <meta charset="utf-8">
    <title>Laporan</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mine/root.css') }}" rel="stylesheet">
    <link href="{{ asset('fontawesome/css/all.min.css') }}" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="table-responsive">
                    <table class="table text-center ver-cen" border="1">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>User ID</th>
                                <th>Nama User</th>
                                <th>Kode Pesanan</th>
                                <th>Product ID</th>
                                <th>Jumlah</th>
                                <th>Total Harga</th>
                                <th>Biaya Admin</th>
                                <th><strong>Biaya yang harus dibayar</strong></th>
                                <th>Tanggal Order</th>
                                <th>Tanggal Lunas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1 ?>
                            @forelse ($laporans as $laporan)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $laporan->id }}</td>
                                <td>{{ $laporan->name }}</td>
                                <td>{{ $laporan->kode_pesanan }}</td>
                                <td>{{ $laporan->product_id }}</td>
                                <td>{{ $laporan->jumlah_pesanan }}</td>
                                <td>Rp. {{ number_format($laporan->total_harga) }}</td>
                                <td>Rp. {{ number_format($laporan->biaya_admin) }}</td>
                                <td>Rp. {{ number_format($laporan->total_harga + $laporan->biaya_admin) }}</td>
                                <td>{{ $laporan->created_at }}</td>
                                <td>{{ $laporan->updated_at }}</td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="11">Data Kosong</td>
                            </tr>
                            @endforelse

                            @if (!empty($laporan))
                            <tr>
                                <td colspan="10" align="right"><strong>Total Pembelian: </strong></td>
                                <td align="right"><strong>{{ $laporan->count()  }}</strong></td>
                            </tr>
                            <tr>
                                <td colspan="10" align="right"><strong>Total Pendapatan: </strong></td>
                                <td align="right"><strong>Rp. {{ number_format($laporan->sum('total_harga') + $laporan->sum('biaya_admin')) }}</strong></td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

</body>

</html>
