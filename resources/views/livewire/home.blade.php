<div class="container">

    {{-- Banner --}}
    <div class="banner">
        <img src="{{ url('assets/slider/banner.jpeg') }}" />
    </div>

    {{-- Liga --}}
    <div class="pilih-liga mt-4">
        <h3>
            <strong>Pilih Gender</strong>
            <a href="{{ route('products') }}" class="btn btn-dark float-right"><i class="fas fa-eye"></i> Lihat semua</a>
        </h3>
        <div class="row mt-4">
            @foreach($genders as $gender)
            <div class="col">
                <a href="{{ route('product.gender', $gender->id) }}">
                    <div class="card shadow">
                        <div class="card-body text-center card-anchor-text">
                            <h3>{{  ucwords($gender->nama) }}</h3>
                            <img src="{{ url('assets/shoes') }}/{{ $gender->gambar }}" class="img-fluid mt-3" />
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>

    {{-- Best --}}
    <section class="products mt-5 mb-5">
        <h3><strong>Produk Terbaik</strong></h3>
        <div class="row mt-4">
            @foreach($products as $product)
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body text-center">
                        <img src="{{ asset('storage/photos') }}/{{ $product->gambar }}" class="img-fluid" />
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <h5><strong>{{ $product->nama }}</strong></h5>
                                <p>Rp. {{ number_format($product->harga) }}</p>
                            </div>
                        </div>
                        <div class="row nt-2">
                            <div class="col-md-12">
                                <a href="{{ route('product.detail', $product->id) }}" class="btn btn-dark btn-block"><i class="fas fa-eye"></i> Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </section>
</div>
