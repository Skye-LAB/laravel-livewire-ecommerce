<div class="container">
  <div class="row mb-2 mt-4">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a class="text-dark" href="{{ route('home') }}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="row">
    <div class="col">
      @if (session()->has('message'))
      <div class="alert alert-success">
        {{ session('message') }}
      </div>
      @endif
    </div>
  </div>

  <div class="row justify-content-center">
    <div class="col-md-6">
      <form>
        <div class="form-group">
          <label for="namaProduct">Nama Product</label>
          <input id="namaProduct" type="text" class="form-control @error('nama') is-invalid @enderror" wire:model="nama" autofocus>

          @error('nama')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label for="gender">Select Gender</label>
          <select wire:model="gender" class="form-control @error('gender') is-invalid @enderror custom-select" id="gender">
            <option>-- Pilih Gender --</option>
            @foreach ($genders as $gender)
            <option value="{{ $gender->nama }}">{{ ucwords($gender->nama) }}</option>
            @endforeach
          </select>
          @error('gender')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label for="stockProduct">Product Stock</label>
          <input wire:model="stock" type="number" class="form-control @error('stock') is-invalid @enderror" id="stockProduct">
          @error('harga')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label for="hargaProduct">Harga</label>
          <input wire:model="harga" type="number" class="form-control @error('harga') is-invalid @enderror" id="hargaProduct">

          @error('harga')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label for="beratProduct">Berat</label>
          <input id="beratProduct" type="text" class="form-control @error('berat') is-invalid @enderror" wire:model="berat" autofocus>

          @error('berat')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <!-- <div class="form-group">
             <label for="gambar">Gambar</label>
             <input wire:model="photo" type="file" class="form-control-file" id="gambar">
             @error('photo')
             <div class="alert alert-danger mt-2">
             {{ $message }}
             </div>
             @enderror
             </div> -->
        <div class="form-group">
          <label for="gambar">Gambar</label>
          <div class="input-group mb-3">
            <div class="custom-file">
                <input type="file" class="custom-file-input" wire:model="photo">
                <!-- <input type="file" class="custom-file-input"
                     @if ($title == 'Tambah Product')
                     wire:model="photo"
                     @else
                     wire:model="oldPhoto"
                     @endif> -->
              <label class="custom-file-label">{{ $photo ? $photo->getClientOriginalName() : $oldPhoto }}</label>
            </div>
          </div>
          @error('photo')
          <div class="alert alert-danger mt-2">
            {{ $message }}
          </div>
          @enderror
        </div>

        @if ($photo || $oldPhoto)
        <div class="row">
          <div class="col">
            Photo Preview:
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Preview Gambar</h5>
              </div>
              @if ($title == 'Tambah Product')
              <img src="{{ $photo->temporaryUrl() }}" class="img-fluid">
              @elseif ($title == 'Edit Product' && $photo)
              <img src="{{ $photo->temporaryUrl() }}" class="img-fluid">
              @else
              <img src="{{ asset('storage/photos/' . $oldPhoto) }}" class="img-fluid">
              @endif
            </div>
          </div>
        </div>
        @endif
        <div class="row">
          <div class="col">
            @if ($title == 'Tambah Product')
            <button wire:click.prevent="save" type="submit" class="btn btn-success btn-block mb-2 mt-2">Save</button>
            @else
            <button wire:click.prevent="edit({{ $product->id }})" type="submit" class="btn btn-success btn-block mb-2 mt-2">Edit</button>
            @endif
          </div>
          <div class="col">
            <button wire:click.prevent="resetForm" type="reset" class="btn btn-danger btn-block mb-2 mt-2">Reset</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
