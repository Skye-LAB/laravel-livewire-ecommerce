<div class="container">
    <div class="row mb-2 mt-4">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="text-dark" href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List Pegawai</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @elseif (session()->has('warning'))
                <div class="alert alert-danger">
                    {{ session('warning') }}
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="table-responsive">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>ID Pegawai</th>
                            <th>Nama Pegawai</th>
                            <th>Email Pegawai</th>
                            <th>Level</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1?>
                        @forelse ($pegawais as $pegawai)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $pegawai->id }}</td>
                            <td>{{ $pegawai->name }}</td>
                            <td>{{ $pegawai->email }}</td>
                            <td>{{ $pegawai->level }}</td>
                            <td>
                                @if ($pegawai->level != 'admin')
                                    <a role="button" wire:click.prevent="jadiAdmin({{ $pegawai->id }})"><i class="fas fa-arrow-up text-success"></i></a>
                                @else
                                    -
                                @endif
                                <br>
                                <a role="button" wire:click.prevent="delete({{ $pegawai->id }})"><i class="fas fa-trash text-danger"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">Data Kosong</td>
                        </tr>
                        @endforelse

                        @if (!empty($pegawai))
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
