<div class="container">
    <div class="row mb-2 mt-4">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="text-dark" href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item"><a class="text-dark" href="{{ route('products') }}">List Sepatu</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Detail Sepatu</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @elseif (session()->has('warning'))
                <div class="alert alert-danger">
                    {{ session('warning') }}
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card gambar-product">
                <div class="card-body">
                    <img src="{{ asset('storage/photos') }}/{{ $product->gambar }}" class="img-fluid" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h2>
                <strong>{{ $product->nama }}</strong>
                @if ($product->stock > 0)
                <span class="badge badge-success"> <i class="fas fa-check"></i> Stok Ada</span>
                @else
                <span class="badge badge-danger"> <i class="fas fa-times"></i> Stok Habis</span>
                @endif
            </h2>
            <h4>Rp. {{ number_format($product->harga) }}</h4>

            <div clas="row">
                <div class="col">
                    <form wire:submit.prevent="masukkanKeranjang">
                        <table class="table" style="border-top: hidden">
                            <tr>
                                <td>Gender</td>
                                <td>:</td>
                                <td>
                                    {{ ucwords($product->gender->nama) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Jenis</td>
                                <td>:</td>
                                <td>
                                    {{ $product->jenis }}
                                </td>
                            </tr>
                            <tr>
                                <td>Stock</td>
                                <td>:</td>
                                <td>
                                    {{ $product->stock }}
                                </td>
                            </tr>
                            <tr>
                                <td>Berat</td>
                                <td>:</td>
                                <td>
                                    {{ number_format($product->berat, 2) }} Kg
                                </td>
                            </tr>
                            <tr>
                                <td>Jumlah</td>
                                <td>:</td>
                                <td>
                                    <input id="jumlah_pesanan" type="number" class="form-control @error('jumlah_pesanan') is-invalid @enderror" wire:model="jumlah_pesanan"
                                           value="{{ old('jumlah_pesanan') }}" autofocus>

                                    @error('jumlah_pesanan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <button class="btn btn-dark btn-block" type="sumbit" @if ($product->stock == 0) disabled @endif><i class="fas fa-shopping-cart"></i> Masukkan Ke Keranjang</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
