<div class="container">
    <div class="row mb-2 mt-4">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="text-dark" href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">History</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col">
            @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="table-responsive">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Tanggal Pesan</th>
                            <th>Kode Pemesanan</th>
                            <th>Pesanan</th>
                            <th>Status</th>
                            <th><strong>Total Harga</strong></th>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1 ?>
                        @forelse ($pesanans as $pesanan)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $pesanan->created_at }}</td>
                            <td>{{ $pesanan->kode_pesanan }}</td>
                            <td>
                                <?php $pesanan_details = \App\Models\PesananDetail::where('pesanan_id', $pesanan->id)->get() ?>
                                @foreach ($pesanan_details as $pesanan_detail)
                                <img src="{{ asset('storage/photos') }}/{{ $pesanan_detail->product->gambar }}" class="img-fluid" width="50" />
                                {{ $pesanan_detail->product->nama }}
                                <br>
                                @endforeach
                            </td>
                            <td>
                                @if ($pesanan->status == 1 && $pesanan->status != 0)
                                Belum Lunas
                                @else
                                Lunas
                                @endif
                            </td>
                            <td><strong>Rp. {{ number_format($pesanan->total_harga) }}</strong></td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="7">Data Kosong</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-body">
                    <p>Untuk pembayaran silahkan transfer ke rekening dibawah ini :</p>
                    <div class="media">
                        <img src="{{ url('assets/bri.png') }}" class="mr-3" alt="..." width="60">
                        <div class="media-body">
                            <h5 class="mt-0">Bank BRI</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
