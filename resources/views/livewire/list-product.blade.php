<div class="container">
    <div class="row mb-2 mt-4">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="text-dark" href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List Product</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col">
            @if (session()->has('message'))
                <div class="alert alert-danger">
                    {{ session('message') }}
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <a href="{{ route('tambah.product') }}" class="btn btn-success float-right mb-4">Tambah Product</a>
            <div class="table-responsive">
                <table class="table text-center ver-cen">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Gambar</th>
                            <th>Stock</th>
                            <th>Harga</th>
                            <th>Berat</th>
                            <th>Gender</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1?>
                        @forelse ($products as $product)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $product->nama }}</td>
                            <td>
                                <img src="{{ asset('storage/photos') }}/{{ $product->gambar }}" class="img-fluid" width="150"/>
                            </td>
                            <td>{{ $product->stock }}</td>
                            <td>{{ $product->harga }}</td>
                            <td>{{ number_format($product->berat) }} Kg</td>
                            <td>{{ ucwords($product->gender->nama) }}</td>
                            <td>
                                <a role="button" href="{{ route('edit.product', $product->id) }}"><i class="fas fa-pencil-alt text-success"></i></a>
                                <br>
                                <a role="button" wire:click.prevent="delete({{ $product->id }})"><i class="fas fa-trash text-danger"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="7">Data Kosong</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
