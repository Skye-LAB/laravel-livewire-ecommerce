<div class="container">
    <div class="row mb-2 mt-4">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="text-dark" href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item"><a class="text-dark" href="{{ route('keranjang') }}">Keranjang</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Check out</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col">
            @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <a href="{{ route('keranjang') }}" class="btn btn-sm btn-dark"><i class="fas fa-arrow-left"></i> Kembali</a>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col">
            <h4>Informasi Pembayaran</h4>
            <hr>
            <p>Untuk pembayaran silahkan transfer ke rekening dibawah ini sebesar: <strong>Rp. {{ number_format($total_harga) }}</strong></p>
            <div class="media">
                <img src="{{ url('assets/bri.png') }}" class="mr-3" alt="..." width="60">
                <div class="media-body">
                    <h5 class="mt-0">Bank BRI</h5>
                </div>
            </div>
        </div>
        <div class="col">
            <h4>Informasi Pengiriman</h4>
            <hr>
            <form wire:submit.prevent="checkout">
                <div class="form-group">
                    <label>No. Hp: </label>
                    <input id="nohp" type="text" class="form-control @error('nohp') is-invalid @enderror" wire:model="nohp" value="{{ old('nohp') }}"
                        autofocus>

                    @error('nohp')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Alamat: </label>
                    <textarea wire:model="alamat" class="form-control @error('alamat') is-invalid @enderror" wire:model="alamat" value="{{ old('alamat') }}">
                    </textarea>

                    @error('alamat')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <button type="submit" class="btn btn-success btn-block">Check Out <i class="fas fa-arrow-right"></i></button>
            </form>
        </div>
    </div>
</div>
