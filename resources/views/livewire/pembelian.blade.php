<div class="container">
    <div class="row mb-2 mt-4">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="text-dark" href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pembelian User</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="table-responsive">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama User</th>
                            <th>Alamat</th>
                            <th>No. Hp</th>
                            <th>Kode Pesanan</th>
                            <th>Biaya Admin</th>
                            <th>Harga</th>
                            <th><strong>Total Harga</strong></th>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1?>
                        @forelse ($pesanans as $pesanan)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>
                                {{ $pesanan->user->name }}
                            </td>
                            <td>
                                {{ $pesanan->user->alamat }}
                            </td>
                            <td>
                                {{ $pesanan->user->nohp }}
                            </td>
                            <td>
                                {{ $pesanan->kode_pesanan }}
                            </td>
                            <td>
                                Rp. {{ number_format($pesanan->biaya_admin, 2) }}
                            </td>
                            <td>
                                Rp. {{ number_format($pesanan->total_harga, 2) }}
                            </td>
                            <td>
                                Rp. {{ number_format($pesanan->total_harga + $pesanan->biaya_admin, 2) }}
                            </td>
                            <td>
                                @if ($pesanan->status == 1)
                                    <i role="button" wire:click="done({{ $pesanan->id }})" class="fas fa-check text-success"></i>
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="9">Data Kosong</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
