<div class="container">
    <div class="row mb-2 mt-4">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="text-dark" href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Keranjang</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col">
            @if (session()->has('message'))
                <div class="alert alert-danger">
                    {{ session('message') }}
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="table-responsive">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Gambar</th>
                            <th>Keterangan</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th><strong>Total Harga</strong></th>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1?>
                        @forelse ($pesanan_details as $pesanan_detail)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>
                                <img src="{{ asset('storage/photos') }}/{{ $pesanan_detail->product->gambar }}" class="img-fluid" width="200"/>
                            </td>
                            <td>
                                {{ $pesanan_detail->product->nama }}
                            </td>
                            <td>{{ $pesanan_detail->jumlah_pesanan }}</td>
                            <td>Rp. {{ number_format($pesanan_detail->product->harga) }}</td>
                            <td><strong>{{ $pesanan_detail->total_harga }}</strong></td>
                            <td>
                                <i role="button" wire:click="destroy({{ $pesanan_detail->id }})" class="fas fa-trash text-danger"></i>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">Data Kosong</td>
                        </tr>
                        @endforelse

                        @if (!empty($pesanan))
                        <tr>
                            <td colspan="5" align="right"><strong>Total Harga: </strong></td>
                            <td align="right"><strong>Rp. {{ number_format($pesanan->total_harga) }}</strong></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="right"><strong>Biaya Admin: </strong></td>
                            <td align="right"><strong>Rp. {{ number_format($pesanan->biaya_admin) }}</strong></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="right"><strong>Harga yang harus dibayar: </strong></td>
                            <td align="right"><strong>Rp. {{ number_format($pesanan->total_harga + $pesanan->biaya_admin) }}</strong></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="5"></td>
                            <td colspan="2">
                                <a href="{{ route('checkout') }}" class="btn btn-success btn-block">
                                    Check Out
                                    <i class="fas fa-arrow-right"></i>
                                </a>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
