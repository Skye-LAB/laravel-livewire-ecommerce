<div class="container">
    <div class="row mb-2 mt-4">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a class="text-dark" href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Laporan</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-md-4">
                    <a target="_blank" href="{{ URL::to('/laporan/pdf') }}" class="btn btn-danger btn-block">Export to PDF (Semua)</a>
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <a target="_blank" href="{{ URL::to('/laporan/pdf') }}/{{ $bulan }}" class="btn btn-success btn-block">Export to PDF (Bulanan)</a>
                    <select class="custom-select mt-2" wire:model="bulan">
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>
            </div>
            <div class="table-responsive mt-4">
                <table class="table text-center ver-cen">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>User ID</th>
                            <th>Nama User</th>
                            <th>Kode Pesanan</th>
                            <th>Product ID</th>
                            <th>Jumlah</th>
                            <th>Total Harga</th>
                            <th>Biaya Admin</th>
                            <th><strong>Biaya yang harus dibayar</strong></th>
                            <th>Tanggal Order</th>
                            <th>Tanggal Lunas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1 ?>
                        @forelse ($laporans as $laporan)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $laporan->id }}</td>
                            <td>{{ $laporan->name }}</td>
                            <td>{{ $laporan->kode_pesanan }}</td>
                            <td>{{ $laporan->product_id }}</td>
                            <td>{{ $laporan->jumlah_pesanan }}</td>
                            <td>Rp. {{ number_format($laporan->total_harga) }}</td>
                            <td>Rp. {{ number_format($laporan->biaya_admin) }}</td>
                            <td>Rp. {{ number_format($laporan->total_harga + $laporan->biaya_admin) }}</td>
                            <td>{{ $laporan->created_at }}</td>
                            <td>{{ $laporan->updated_at }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="11">Data Kosong</td>
                        </tr>
                        @endforelse

                        @if (!empty($laporan))
                        <tr>
                            <td colspan="10" align="right"><strong>Total Pembelian: </strong></td>
                            <td align="right"><strong>{{ $laporan->count()  }}</strong></td>
                        </tr>
                        <tr>
                            <td colspan="10" align="right"><strong>Total Pendapatan: </strong></td>
                            <td align="right"><strong>Rp. {{ number_format($laporan->sum('total_harga') + $laporan->sum('biaya_admin')) }}</strong></td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
