-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 17, 2020 at 09:05 AM
-- Server version: 10.5.5-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lspbayu`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`skye`@`localhost` PROCEDURE `jumlahProductTerjual` ()  BEGIN
	SELECT sum(jumlah_pesanan) AS product_terjual, product_id
    FROM pesanan_details GROUP BY product_id;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `nama`, `gambar`, `created_at`, `updated_at`) VALUES
(1, 'male', 'maleShoes.jpeg', NULL, NULL),
(2, 'female', 'femaleShoes.jpeg', NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `laporan_views`
-- (See below for the actual view)
--
CREATE TABLE `laporan_views` (
`name` varchar(255)
,`id` bigint(20) unsigned
,`kode_pesanan` varchar(255)
,`total_harga` int(11)
,`biaya_admin` int(11)
,`jumlah_pesanan` int(11)
,`product_id` int(11)
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(70, '2014_10_12_000000_create_users_table', 1),
(71, '2014_10_12_100000_create_password_resets_table', 1),
(72, '2019_08_19_000000_create_failed_jobs_table', 1),
(73, '2020_11_14_104407_create_products_table', 1),
(74, '2020_11_14_104445_create_pesanans_table', 1),
(75, '2020_11_14_104453_create_pesanan_details_table', 1),
(76, '2020_11_14_104503_create_genders_table', 1),
(77, '2020_11_15_065333_create_laporan_views_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pesanans`
--

CREATE TABLE `pesanans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_pesanan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `total_harga` int(11) NOT NULL,
  `biaya_admin` int(11) NOT NULL DEFAULT 500,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pesanans`
--

INSERT INTO `pesanans` (`id`, `kode_pesanan`, `status`, `total_harga`, `biaya_admin`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'LR-1', '2', 125000, 500, 2, '2020-11-16 09:04:16', '2020-11-16 09:05:49'),
(2, 'LR-2', '2', 1000000, 500, 1, '2020-11-16 18:56:36', '2020-11-16 18:57:04');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan_details`
--

CREATE TABLE `pesanan_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jumlah_pesanan` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `pesanan_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pesanan_details`
--

INSERT INTO `pesanan_details` (`id`, `jumlah_pesanan`, `total_harga`, `product_id`, `pesanan_id`, `created_at`, `updated_at`) VALUES
(1, 1, 125000, 1, 1, '2020-11-16 09:04:16', '2020-11-16 09:04:16'),
(2, 5, 1000000, 3, 2, '2020-11-16 18:56:37', '2020-11-16 18:56:37');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL DEFAULT 125000,
  `stock` int(11) NOT NULL DEFAULT 0,
  `jenis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Replika Ori',
  `berat` double(8,2) NOT NULL DEFAULT 0.50,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `nama`, `harga`, `stock`, `jenis`, `berat`, `gambar`, `gender_id`, `created_at`, `updated_at`) VALUES
(1, 'Sepatu 1', 125000, 29, 'Replika Ori', 1.00, '5rtLfMMRdqt9wNuYIsCZE46FMuQlckeacckTq02M.jpeg', 1, '2020-11-16 09:04:07', '2020-11-16 09:04:16'),
(2, 'Sepatu 2', 150000, 0, 'Replika Ori', 1.00, 'a4WPZoE0Q7bwQwo6EV7dQJS1Ch0mz7lieFTQStjX.jpeg', 1, '2020-11-16 18:55:40', '2020-11-16 18:55:40'),
(3, 'Sepatu 3', 200000, 95, 'Replika Ori', 1.00, 'Hy51sRHGjcxjaEtwlMnHeZGkFJVlOiV0tpJKe5lO.jpeg', 2, '2020-11-16 18:56:10', '2020-11-16 18:56:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nohp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `level`, `password`, `alamat`, `nohp`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'skyeUser', 'user@gmail.com', NULL, 'user', '$2y$10$dWie1cDJqbUr5Z6w4pVPAuJddyHXlUGpg7RQFbasBga53h7rDSoDK', 'Dis', '08122121231', NULL, NULL, '2020-11-16 18:56:47'),
(2, 'skyePegawai', 'pegawai@gmail.com', NULL, 'pegawai', '$2y$10$l7J/NaYVUWrvLbshQ56.juTEbMJ1877IvVNoe9SSp0dK.sDb/FStS', 'Disana', '08122121231', NULL, NULL, '2020-11-16 16:22:04'),
(3, 'skyeAdmin', 'admin@gmail.com', NULL, 'admin', '$2y$10$ZvXfFk7H7OdDTKXRSNlmi.ycNI0.HMITtcQVZHUaqVPJxl3DNXeva', NULL, NULL, NULL, NULL, NULL),
(6, 'Pegawai', 'pegawai1@gmail.com', NULL, 'user', '$2y$10$TUe1V/DNMl3giZW6km5tj.7WQmg4OQax.Lq1i5sI0OKgtr0TsO5mS', NULL, NULL, NULL, '2020-11-16 18:44:54', '2020-11-16 18:44:54');

-- --------------------------------------------------------

--
-- Structure for view `laporan_views`
--
DROP TABLE IF EXISTS `laporan_views`;

CREATE ALGORITHM=UNDEFINED DEFINER=`skye`@`localhost` SQL SECURITY DEFINER VIEW `laporan_views`  AS  (select `users`.`name` AS `name`,`users`.`id` AS `id`,`pesanans`.`kode_pesanan` AS `kode_pesanan`,`pesanans`.`total_harga` AS `total_harga`,`pesanans`.`biaya_admin` AS `biaya_admin`,`pesanan_details`.`jumlah_pesanan` AS `jumlah_pesanan`,`pesanan_details`.`product_id` AS `product_id`,`pesanans`.`created_at` AS `created_at`,`pesanans`.`updated_at` AS `updated_at` from ((`users` join `pesanans` on(`users`.`id` = `pesanans`.`user_id`)) join `pesanan_details` on(`pesanans`.`id` = `pesanan_details`.`pesanan_id`)) where `pesanans`.`status` = 2) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pesanans`
--
ALTER TABLE `pesanans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesanan_details`
--
ALTER TABLE `pesanan_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `pesanans`
--
ALTER TABLE `pesanans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pesanan_details`
--
ALTER TABLE `pesanan_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
