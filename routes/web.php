<?php

use App\Http\Controllers\DownloadPDF;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::livewire('/', 'home')->name('home');
Route::livewire('/products', 'product-index')->name('products');
Route::livewire('/products/gender/{genderId}', 'product-gender')->name('product.gender');
Route::livewire('/products/{id}', 'product-detail')->name('product.detail');
Route::livewire('/keranjang', 'keranjang')->name('keranjang');
Route::livewire('/checkout', 'checkout')->name('checkout');
Route::livewire('/history', 'history')->name('history');

Route::group(['middleware' => ['auth', 'cekLevel:pegawai']], function() {
    Route::livewire('/pembelian', 'pembelian')->name('pembelian');
    Route::livewire('/tambah', 'tambah-product')->name('tambah.product');
    Route::livewire('/edit/{id}', 'tambah-product')->name('edit.product');
    Route::livewire('/list', 'list-product')->name('list.product');
});

Route::group(['middleware' => ['auth', 'cekLevel:admin']], function() {
    Route::livewire('/laporan', 'laporan')->name('laporan');
    Route::get('/laporan/pdf', [DownloadPDF::class, 'createPDFAll']);
    Route::get('/laporan/pdf/{month}', [DownloadPDF::class, 'createPDFMonthly']);
    Route::livewire('/pegawai', 'role-update')->name('list.pegawai');
});
