<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'nama' => 'Sepatu 1',
            'gender_id' => 1,
            'gambar' => 'maleShoes.jpeg'
        ]);

        DB::table('products')->insert([
            'nama' => 'Sepatu 2',
            'gender_id' => 2,
            'gambar' => 'femaleShoes.jpeg'
        ]);
    }
}
