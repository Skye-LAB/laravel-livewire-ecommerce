<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'skyeUser',
            'email' => 'user@gmail.com',
            'password' => Hash::make('asdasdqwe'),
            'level' => 'user'
        ]);

        DB::table('users')->insert([
            'name' => 'skyePegawai',
            'email' => 'pegawai@gmail.com',
            'password' => Hash::make('asdasdzxc'),
            'level' => 'pegawai'
        ]);

        DB::table('users')->insert([
            'name' => 'skyeAdmin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('asdzxczxc'),
            'level' => 'admin'
        ]);
    }
}
