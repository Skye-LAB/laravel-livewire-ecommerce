<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateLaporanViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW laporan_views AS (
            SELECT users.name, users.id, pesanans.kode_pesanan,
            pesanans.total_harga, pesanans.biaya_admin, pesanan_details.jumlah_pesanan,
            pesanan_details.product_id, pesanans.created_at, pesanans.updated_at FROM users JOIN pesanans ON users.id = pesanans.user_id
            JOIN pesanan_details ON pesanans.id = pesanan_details.pesanan_id WHERE pesanans.status = 2
        )");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_views');
    }
}
